CC = clang
CXX = clang++
#CCFLAGS = -Wall -Wextra -O0 -pedantic -march=native -mtune=native -g -pg
CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native
CXXFLAGS = $(CCFLAGS)

DEPS = Makefile
OBJ = obj

$(OBJ)/%.o: %.c %.h $(DEPS)
	mkdir -p $(OBJ)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(OBJ)/%.o: %.cpp %.hpp $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: knit

knit: knit.c $(OBJ)/digraph.o $(OBJ)/ar_quiver.o
	$(CC) $(CCFLAGS) knit.c $(OBJ)/digraph.o $(OBJ)/ar_quiver.o -o knit

.PHONY: clean bin_clean obj_clean
clean: bin_clean obj_clean
bin_clean:
	rm -fv p01 p02 test
obj_clean:
	rm -rfv $(OBJ)/*
