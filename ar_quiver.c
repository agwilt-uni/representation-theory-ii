#define _POSIX_C_SOURCE 201112L

#include "ar_quiver.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void ar_quiver_print(const struct ar_quiver *G)
{
	printf("Vertices with dimension vectors (valuation always 1):\n");
	for (int i = 0; i < G->graph.n; ++i) {
		printf("\t%d: ", i);
		dim_vector_print(G->num_quiver_vertices, G->graph.V[i].dim_vector);
		printf(" in ");
		if (G->graph.V[i].min_delta < INT_MAX)
			printf("%dΔ, ", G->graph.V[i].min_delta);
		if (G->graph.V[i].min_delta_proj < INT_MAX)
			printf("%dΔ(proj), ", G->graph.V[i].min_delta_proj);
		if (G->graph.V[i].min_delta_proj_tau_minus < INT_MAX)
			printf("%dΔ(proj, τ⁻), ", G->graph.V[i].min_delta_proj_tau_minus);
		if (G->graph.V[i].tau == -1)
			printf("projective\n");
		else
			printf("τ = %d\n", G->graph.V[i].tau);
	}
	printf("Arrows:\n");
	for (int i = 0; i < G->graph.m; ++i) {
		printf(
			"\t%d→%d with valuation %d\n",
			G->graph.E[i].x,
			G->graph.E[i].y,
			G->graph.E[i].valuation
		);
	}
}

struct ar_quiver knit(
	struct graph const* const Q,
	int *const *ind_proj_dim,
	int *const *ind_proj_radical_decomp,
	int const max_n
)
{
	struct ar_quiver G = {
		.graph = {.n=0, .m=0, ._max_n=0, ._max_m=0, .V=NULL, .E=NULL},
		.num_quiver_vertices = Q->n
	};
	int *projective_in_quiver = malloc(Q->n * sizeof(int));
	for (int i = 0; i < Q->n; ++i)
		projective_in_quiver[i] = -1; // not in quiver

	// I0
	for (int i = 0; i < Q->n; ++i) {
		if (dim_vector_total_dimension(Q->n, ind_proj_dim[i]) == 1) {
			if (dim_vector_total_dimension(Q->n, ind_proj_radical_decomp[i]))
				exit(42);
			// P(i) is a simple projective
			int const v = graph_add_nodes(&G.graph, 1);
			G.graph.V[v].min_delta = 0;
			G.graph.V[v].min_delta_proj = INT_MAX;
			G.graph.V[v].min_delta_proj_tau_minus = INT_MAX;
			G.graph.V[v].tau = -1;
			G.graph.V[v].dim_vector = dim_vector_copy(Q->n, ind_proj_dim[i]);
			projective_in_quiver[i] = v;

			printf("I0: Added simple projective vertex %d ", v);
			dim_vector_print(Q->n, ind_proj_dim[i]);
			putchar('\n');
		}
	}

	// II0
	for (int v = 0; v < G.graph.n; ++v) {
		G.graph.V[v].min_delta_proj = 0;
		if (G.graph.V[v].min_delta == INT_MAX) continue;
		for (int i = 0; i < Q->n; ++i) {
			// Check if v direct summand of rad(P(i))
			/*
			printf("Checking if %d ", v);
			dim_vector_print(Q->n, G.graph.V[v].dim_vector);
			printf(" is a direct summand of rad(P(%d)):\n", i);
			*/
			for (int j = 0; j < Q->n; ++j) {
				if (ind_proj_radical_decomp[i][j] == 0) { continue; }
				bool const v_is_ind_proj_j = (v == projective_in_quiver[j]);
				bool const v_is_ind_proj_j_test = dim_vectors_equal(
					Q->n,
					G.graph.V[v].dim_vector,
					ind_proj_dim[j]
				);
				if (v_is_ind_proj_j != v_is_ind_proj_j_test) exit(44);
				if (v == projective_in_quiver[j]) {
					if (projective_in_quiver[i] == -1) {
						int const w = graph_add_nodes(&G.graph, 1);
						G.graph.V[w].min_delta = INT_MAX;
						G.graph.V[w].min_delta_proj = 0;
						G.graph.V[w].min_delta_proj_tau_minus = INT_MAX;
						G.graph.V[w].tau = -1;
						G.graph.V[w].dim_vector = dim_vector_copy(Q->n, ind_proj_dim[i]);
						projective_in_quiver[i] = w;

						printf("II0: Added projective vertex P(%d) =  %d ", i, w);
						dim_vector_print(Q->n, ind_proj_dim[i]);
						putchar('\n');
					}
					graph_add_edge(
						&G.graph,
						v,
						projective_in_quiver[i],
						ind_proj_radical_decomp[i][j]
					);
				}
			}
		}
	}

	// III0
	for (int v = 0; v < G.graph.n; ++v) {
		if (G.graph.V[v].min_delta_proj <= 0) // ₀Δ(proj, τ) extends ₀Δ(proj)
			G.graph.V[v].min_delta_proj_tau_minus = 0;
		if (G.graph.V[v].min_delta > 0) continue; // Not in ₀Δ
		if (is_injective(Q->n, ind_proj_dim, G.graph.V[v].dim_vector)) continue;

		int const w = graph_add_nodes(&G.graph, 1);
		G.graph.V[w].min_delta = INT_MAX;
		G.graph.V[w].min_delta_proj = INT_MAX;
		G.graph.V[w].min_delta_proj_tau_minus = 0;
		G.graph.V[w].tau = v;
		G.graph.V[w].dim_vector = calloc(Q->n, sizeof(int));
		dim_vector_subtract_from(Q->n, G.graph.V[w].dim_vector, G.graph.V[v].dim_vector);
		for (int neighbour = 0; neighbour < G.graph.V[v].d_plus; ++neighbour) {
			const struct edge *e = G.graph.E + G.graph.V[v].to[neighbour];
			dim_vector_multiply_and_add_to(
				Q->n,
				G.graph.V[w].dim_vector,
				e->valuation,
				G.graph.V[e->y].dim_vector
			);
			graph_add_edge(&G.graph, e->y, w, e->valuation);
		}
		printf("III0: Added vertex τ⁻(%d) = %d ", v, w);
		dim_vector_print(Q->n, G.graph.V[w].dim_vector);
		putchar('\n');
		for (int i = 0; i < Q->n; ++i) {
			if (G.graph.V[w].dim_vector[i] < 0) {
				fprintf(stderr, "INTERAL ERROR: Negative dimension vector\n");
				exit(2);
			}
		}
	}

	for (int n = 1; n <= max_n; ++n) {
		// In
		bool new_vertices_in_n_delta = false;
		for (int v = 0; v < G.graph.n; ++v) {
			if (G.graph.V[v].min_delta_proj > n-1) continue;
			bool v_in_n_delta = true;
			// Check if all parents u of v in ₙ₋₁Δ(proj) are in ₙ₋₁Δ
			for (int pred = 0; pred < G.graph.V[v].d_minus; ++pred) {
				int const u = G.graph.E[G.graph.V[v].from[pred]].x;
				if (G.graph.V[u].min_delta_proj <= n-1) continue;
				if (G.graph.V[u].min_delta > n-1) {
					v_in_n_delta = false;
					break;
				}
			}
			// If v is projective, need to check that all dir.sd. of rad. are in
			int const i = col_of_matrix(Q->n, ind_proj_dim, G.graph.V[v].dim_vector);
			if (i >= 0) {
				for (int j = 0; j < Q->n; ++j) {
					if (ind_proj_radical_decomp[i][j]) {
						int const w = projective_in_quiver[j];
						if ((w == -1) || (G.graph.V[w].min_delta > n-1)) {
							v_in_n_delta = false;
							break;
						}
					}
				}
			}
			if (G.graph.V[v].min_delta <= n-1 && !v_in_n_delta) {
				fprintf(stderr, "INTERNAL ERROR: %dΔ not subset of %dΔ!\n", n-1, n);
				exit(2);
			}
			if (v_in_n_delta && G.graph.V[v].min_delta == INT_MAX) {
				G.graph.V[v].min_delta = n;
				printf("I%d: Vertex %d ", n, v);
				dim_vector_print(Q->n, G.graph.V[v].dim_vector);
				printf(" ∈ %dΔ(proj) newly added to Δ\n", n-1);
				new_vertices_in_n_delta = true;
			}
		}
		if (! new_vertices_in_n_delta) break;

		// IIn
		for (int v = 0; v < G.graph.n; ++v) {
			if (G.graph.V[v].min_delta_proj_tau_minus <= n-1) // ₙΔ(proj) extends ₙ₋₁Δ(proj, τ)
				if (G.graph.V[v].min_delta_proj == INT_MAX)
					G.graph.V[v].min_delta_proj = n;
			if (G.graph.V[v].min_delta > n) continue;
			// Check if v dir. sd. of some P(i)
			for (int i = 0; i < Q->n; ++i) {
				/*
				printf("Checking if %d ", v);
				dim_vector_print(Q->n, G.graph.V[v].dim_vector);
				printf(" is a direct summand of rad(P(%d)):\n", i);
				*/
				for (int j = 0; j < Q->n; ++j) {
					if (ind_proj_radical_decomp[i][j] == 0) { continue; }
					if (v == projective_in_quiver[j]) {
						// v=P(j) is a dir. sd. of P(i)
						if (projective_in_quiver[i] == -1) {
							// P(i) not yet in quiver → add it
							int const w = graph_add_nodes(&G.graph, 1);
							G.graph.V[w].min_delta = INT_MAX;
							G.graph.V[w].min_delta_proj = n;
							G.graph.V[w].min_delta_proj_tau_minus = INT_MAX;
							G.graph.V[w].tau = -1;
							G.graph.V[w].dim_vector = dim_vector_copy(Q->n, ind_proj_dim[i]);
							projective_in_quiver[i] = w;

							printf("II%d: Added projective vertex P(%d) = %d ", n, i, w);
							dim_vector_print(Q->n, ind_proj_dim[i]);
							putchar('\n');
						}
						graph_add_edge(
							&G.graph,
							v,
							projective_in_quiver[i],
							ind_proj_radical_decomp[i][j]
						);
					}
				}
			}
		}

		// IIIn
		for (int v = 0; v < G.graph.n; ++v) {
			if (G.graph.V[v].min_delta_proj <= n)
				if (G.graph.V[v].min_delta_proj_tau_minus == INT_MAX)
					G.graph.V[v].min_delta_proj_tau_minus = n; // ₙΔ(proj, τ) extends ₙΔ(proj)
			if (G.graph.V[v].min_delta != n) continue;
			if (is_injective(Q->n, ind_proj_dim, G.graph.V[v].dim_vector)) continue;
			// Add τ⁻(v) for v in ₙΔ\ₙ₋₁Δ
			int const w = graph_add_nodes(&G.graph, 1);
			G.graph.V[w].min_delta = INT_MAX;
			G.graph.V[w].min_delta_proj = INT_MAX;
			G.graph.V[w].min_delta_proj_tau_minus = n;
			G.graph.V[w].tau = v;
			G.graph.V[w].dim_vector = calloc(Q->n, sizeof(int));
			dim_vector_subtract_from(Q->n, G.graph.V[w].dim_vector, G.graph.V[v].dim_vector);
			for (int child = 0; child < G.graph.V[v].d_plus; ++child) {
				const struct edge *e = G.graph.E + G.graph.V[v].to[child];
				dim_vector_multiply_and_add_to(
					Q->n,
					G.graph.V[w].dim_vector,
					e->valuation,
					G.graph.V[e->y].dim_vector
				);
				graph_add_edge(&G.graph, e->y, w, e->valuation);
			}
			for (int i = 0; i < Q->n; ++i) {
				if (G.graph.V[w].dim_vector[i] < 0) {
					fprintf(stderr, "INTERAL ERROR: Negative dimension vector\n");
					exit(2);
				}
			}

			printf("III%d: Added vertex τ⁻(%d) =  %d ", n, v, w);
			dim_vector_print(Q->n, G.graph.V[w].dim_vector);
			putchar('\n');
		}
	}

	free(projective_in_quiver);
	return G;
}

bool is_injective(int const n, int *const *ind_proj_dim, const int *dim_vector)
{
	// Check if dim_vector is a row in the matrix ind_proj_dim
	for (int j = 0; j < n; ++j) {
		bool is_row_j = true;
		for (int i = 0; i < n; ++i) {
			if (dim_vector[i] != ind_proj_dim[i][j])
				is_row_j = false;
		}
		if (is_row_j)
			return true;
	}
	return false;
}

int col_of_matrix(int n, int *const *M, int const* v)
{
	for (int i = 0; i < n; ++i)
		if (dim_vectors_equal(n, v, M[i]))
			return i;
	return -1;
}

void dim_vector_print(int n, const int *d)
{
	putchar('(');
	for (int i = 0; i < n; ++i) {
		if (i > 0)
			printf(", ");
		printf("%d", d[i]);
	}
	putchar(')');
}

void dim_vector_multiply_and_add_to(int n, int *dest, int const a, const int *src)
{
	for (int i = 0; i < n; ++i)
		dest[i] += a * src[i];
}

void dim_vector_subtract_from(int n, int *dest, const int *src)
{
	for (int i = 0; i < n; ++i)
		dest[i] -= src[i];
}

int dim_vector_total_dimension(int n, const int *d)
{
	int dim = 0;
	for (int i = 0; i < n; ++i)
		dim += d[i];
	return dim;
}

bool dim_vectors_equal(int n, const int *x, const int *y)
{
	for (int i = 0; i < n; ++i)
		if (x[i] != y[i])
			return false;
	return true;
}

int *dim_vector_copy(int n, const int *d)
{
	int *ans = malloc(n * sizeof(int));
	for (int i = 0; i < n; ++i)
		ans[i] = d[i];
	return ans;
}
