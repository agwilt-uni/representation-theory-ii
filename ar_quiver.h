#define _POSIX_C_SOURCE 201112L

#ifndef AR_QUIVER_H
#define AR_QUIVER_H

#include "digraph.h"

struct ar_quiver {
	struct graph graph;
	int num_quiver_vertices;
};

void ar_quiver_print(const struct ar_quiver *G);

struct ar_quiver knit(
	struct graph const* Q, // pointer to quiver
	int *const *ind_proj_dim,
	int *const *ind_proj_radical_decomp,
	int max_n
);

// Is dim.vector of indecomp. module injective?
bool is_injective(int n, int *const *ind_proj_dim, int const* dim_vector);

// Return i s.t. v is the i-th column of M, or else -1
int col_of_matrix(int n, int *const *M, int const* v);

void dim_vector_print(int n, const int *d);
int *dim_vector_copy(int n, const int *d);
void dim_vector_multiply_and_add_to(int n, int *dest, int a, const int *src);
void dim_vector_subtract_from(int n, int *x, const int *y);
int dim_vector_total_dimension(int n, const int *d);
bool dim_vectors_equal(int n, const int *x, const int *y);

#endif // AR_QUIVER_H
