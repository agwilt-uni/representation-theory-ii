#define _POSIX_C_SOURCE 201112L

#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <stdbool.h>

struct vert {
	int id;
	int d_plus, d_minus;
	int _max_plus, _max_minus;
	int *to, *from;			// arrays of edge ids
	// Values should be INT_MAX if not in any of the relevant sets
	int min_delta;
	int min_delta_proj;
	int min_delta_proj_tau_minus;
	int *dim_vector;
	int tau; // -1 if projective
};

// edge (x,y)
struct edge {
	int id;
	int x, y;
	int valuation;
};

struct graph {
	int n, m;
	int _max_n, _max_m;
	struct edge *E;
	struct vert *V;
};


struct graph graph_from_file(char const *filename);
void graph_free(struct graph *G);

/* return new last node */
int graph_add_nodes(struct graph *G, int num);
/* Return value: id of new edge */
int graph_add_edge(struct graph *G, int x, int y, int valuation);

/* WARNING: Worst-case running time |δ⁺(x)| */
bool are_adjacent(struct graph *G, int x, int y);

void graph_print(const struct graph *G);

void graph_print_without_valuation(const struct graph *G);

// Assume min_delta entry used as a map to the original vertex IDs
int *compute_dimension_vector(int quiver_num_vertices, const struct graph *P);

/* Adds ind_proj(x) to graph G, as a separate weakly conn. comp.
 * Quiver G uses min_delta for each vertex to store Id of original.
 * Return value: root of new component in G
 *
 * WARNING: This will cause an infinite loop if our quiver has cycles!
 */
int compute_ind_proj(const struct graph *quiver, int x, struct graph *G, int depth);

#endif
