#define _POSIX_C_SOURCE 201112L

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "digraph.h"
#include "ar_quiver.h"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s acyclic_quiver_file [max_n]\n", argv[0]);
		return 1;
	}
	int const max_n = (argc >= 3) ? atoi(argv[2]) : INT_MAX;

	struct graph Q = graph_from_file(argv[1]);
	printf("=======================\n");
	printf("Quiver:\n");
	graph_print_without_valuation(&Q);
	printf("=======================\n");

	struct graph *ind_proj = calloc(Q.n, sizeof(struct graph));
	int **ind_proj_dim = malloc(Q.n * sizeof(int *));
	// ind_proj_radical_decomp[i][j] is how often P(j) appears as a direct summand in P(i)
	int **ind_proj_radical_decomp = malloc(Q.n * sizeof(int *));

	for (int i = 0; i < Q.n; ++i) {
		if (compute_ind_proj(&Q, i, ind_proj + i, 0) != 0) {
			fprintf(stderr, "Something is horribly wrong\n");
			exit(2);
		}
		ind_proj_dim[i] = compute_dimension_vector(Q.n, ind_proj + i);
		ind_proj_radical_decomp[i] = calloc(Q.n, sizeof(int));
		for (int sd = 0; sd < ind_proj[i].V[0].d_plus; ++sd) {
			int const j = ind_proj[i].V[ind_proj[i].E[ind_proj[i].V[0].to[sd]].y].min_delta;
			++ind_proj_radical_decomp[i][j];
		}
	}

	for (int i = 0; i < Q.n; ++i) {
		printf("P(%d): ", i);
		/*
		graph_print_without_valuation(ind_proj + i);
		printf("Dimension vector ");
		*/
		dim_vector_print(Q.n, ind_proj_dim[i]);
		if (dim_vector_total_dimension(Q.n, ind_proj_dim[i]) == 1) {
			printf(" (simple projective)\n");
			continue;
		}
		printf(", radical ");
		for (int sd = 0; sd < ind_proj[i].V[0].d_plus; ++sd) {
			if (sd > 0) printf(" ⊕ ");
			int const j = ind_proj[i].V[ind_proj[i].E[ind_proj[i].V[0].to[sd]].y].min_delta;
			dim_vector_print(Q.n, ind_proj_dim[j]);
		}
		putchar('\n');
	}
	printf("=======================\n");

	struct ar_quiver G = knit(&Q, ind_proj_dim, ind_proj_radical_decomp, max_n);
	printf("=======================\n");
	printf("\nReachable AR Quiver:\n");
	ar_quiver_print(&G);

	graph_free(&G.graph);
	for (int i = 0; i < Q.n; ++i) {
		graph_free(ind_proj + i);
		free(ind_proj_dim[i]);
		free(ind_proj_radical_decomp[i]);
	}
	free(ind_proj);
	free(ind_proj_dim);
	free(ind_proj_radical_decomp);
	graph_free(&Q);
	return 0;
}
